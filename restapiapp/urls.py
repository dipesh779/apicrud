from django.urls import path
from .views import *


app_name = 'restapiapp'

urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path("clinician/create/api/", ClinicianApiCreateView.as_view(),
         name='clinicianapicreate'),
    path("clinician/<int:pk>/RUD/api/", ClinicianRUDView.as_view(),
         name='clinicianrudapi'),
    path("appointment/api/list-create/",
         AppointmentApiCreateView.as_view(), name='appointmentapicreate'),
    path("appointment/api/<int:pk>/RUD/",
         AppointmentApiRUDView.as_view(), name='appointmentrudapi'),
]
