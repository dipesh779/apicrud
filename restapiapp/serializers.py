from rest_framework import serializers
from .models import *


class ClinicianSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clinician
        fields = "__all__"


class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = ['client_name', 'request_date', 'appointment_date',
                  'appointment_time', 'preferred_clinician',
                  'appointment_reason']
