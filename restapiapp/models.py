from django.db import models


class Clinician(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Appointment(models.Model):
    client_name = models.CharField(max_length=50)
    request_date = models.DateField()
    appointment_date = models.DateField()
    appointment_time = models.TimeField()
    preferred_clinician = models.ForeignKey(
        Clinician, on_delete=models.CASCADE)
    appointment_reason = models.TextField()

    def __str__(self):
        return self.client_name
