from rest_framework.generics import (ListCreateAPIView,
                                     RetrieveUpdateDestroyAPIView)
from django.views.generic import TemplateView
from .serializers import *
from .models import *
from rest_framework.permissions import IsAdminUser


class HomeView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['appointmentlist'] = Appointment.objects.all(
        ).order_by('appointment_date')
        return context


class ClinicianApiCreateView(ListCreateAPIView):
    queryset = Clinician.objects.all()
    serializer_class = ClinicianSerializer


class ClinicianRUDView(RetrieveUpdateDestroyAPIView):
    queryset = Clinician.objects.all()
    serializer_class = ClinicianSerializer


class AppointmentApiCreateView(ListCreateAPIView):
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer
    # permission_classes = [IsAdminUser]


class AppointmentApiRUDView(RetrieveUpdateDestroyAPIView):
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer
    # permission_classes = [IsAdminUser]
