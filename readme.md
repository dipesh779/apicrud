SIMPLE CRUD API WITH DJANGO REST FRAMEWORK

Django REST Framework is a powerful and flexible toolkit for building Web API's.

Requirements

Python 3.7.2 Django 2.2.4 Django REST Framework

how to use

First, we have to start up Django's development server in terminal and then goto browser. python manage.py runserver

for creating Clinician use url 127.0.0.1:8000/clinician/create/api/

for RUD operation of Clinician use url (use id of clinician instead of <int:pk>) 127.0.0.1:8000/clinician/<int:pk>/RUD/api/

for Creating New Appointment use url 127.0.0.1:8000/appointment/api/list-create/

for RUD operation of Appointment use url (use id of appointment instead of <int:pk>) 127.0.0.1:8000/appointment/api/<int:pk>/RUD/


File details
Last updated
17 minutes ago
Lines
34
Size
737 B

0 builds
Code emerging from a folder, with a green tick implying it's correctly working.
It looks like you don't have a build tool set up yet.

Rapidly build, test, and deploy your code using Bitbucket Pipelines.

Your plan already includes build minutes.


Give feedback